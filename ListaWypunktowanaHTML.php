class ListaWypunktowanaHTML
{

    public $htmlOut = null;

    public function __construct()
    {

        $this->htmlOut = "<ul>";

    }

    public function addElement($title=null,$value=null,$bold=true)
    {

        if (empty($value))
        {
            if ($bold == true)
            {
                $this->htmlOut .= "<li><b>" . $title . "</b>:</li>";
            }
            else
            {
                $this->htmlOut .= "<li>" . $title . ":</li>";
            }


        }
        else
        {

            if (is_array($value))
            {

                if (count($value)==1)
                {
                    if ($bold == true)
                    {
                        $this->htmlOut .= "<li><b>" . $title . "</b>: " . $value[0] . "</li>";
                    }
                    else
                    {
                        $this->htmlOut .= "<li>" . $title . ": " . $value[0] . "</li>";
                    }


                }
                else
                {

                    if ($bold == true)
                    {
                        $this->htmlOut .= "<li><b>" . $title . "</b>: <ul>";
                    }
                    else
                    {
                        $this->htmlOut .= "<li>" . $title . ": <ul>";
                    }

                    foreach ($value as $line)
                    {
                        $this->htmlOut .= "<li>" . $line . "</li>";
                    }

                    $this->htmlOut .= "</ul></li>";

                }

            }
            else
            {

                if ($bold == true)
                {
                    $this->htmlOut .= "<li><b>" . $title . "</b>: " . $value . "</li>";
                }
                else
                {
                    $this->htmlOut .= "<li>" . $title . ": " . $value . "</li>";
                }


            }

        }

    }

    public function getHTML()
    {

        return  $this->htmlOut .= "</ul>";

    }




}