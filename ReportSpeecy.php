
class ReportSpeecy
{
    private $fileReport = null;
    public $xmlParser = null;

    public $procesor = array();
    public $ram = array();
    public $plytaGlowna = array();
    public $ekran = array();
    public $kartaGraficzna = array();
    public $dyskTwardy = array();
    public $kartaDzwiekowa = array();
    public $napedOptyczny = array();
    public $kartaSieciowa = array();

    public function __construct($file)
    {

        $this->fileReport = $file;

        $this->xmlParser = simplexml_load_file($file);

        $this->procesorReadInfo();
        $this->ramReadInfo();
        $this->plytaGlownaReadInfo();
        $this->ekranReadInfo();
        $this->dyskTwardyReadInfo();
        $this->kartaDzwiekowaReadInfo();
        $this->kartaSieciowaReadInfo();
        $this->napedOptycznyReadInfo();

    }

    private function getMainsecion ($id)
    {
        return $this->xmlParser->mainsection[$id];
    }

    private function getSection ($mainsection, $type, $value)
    {
        $return = null;

        if ($type == "id")
        {
            $return = $mainsection->section[$value];
        }

        if ($type = "title")
        {
            $howManySection = count($mainsection->section);

            for ($x=0; $x < $howManySection; $x++)
            {

                if ($mainsection->section[$x]['title'] == $value)
                {
                    return $mainsection->section[$x];
                    break;
                }

            }
        }

        return $return;

    }

    private function getData ($section, $title)
    {

        $howMEntry = count($section);

        for ($x=0; $x < $howMEntry; $x++)
        {
            if ($section->entry[$x]['title'] == $title)
            {
                return $section->entry[$x]['value'];
                break;
            }
        }

    }

    private function getDataLastCount ($section)
    {
        $howMany = count($section)-1;

        return $section->entry[$howMany]['value'];
    }

    private function getDataToArray($section,$type)
    {

        /// type: title or value

        $howManyEntries = count($section->entry);

        $temp = array();

        for ($x = 0; $x < $howManyEntries; $x++)
        {

            if ($type = "title")
            {
                $temp[] = (string) $section->entry[$x]['title'];
            }
            else
            {
                $temp[] = (string) $section->entry[$x]['value'];
            }

        }

        return $temp;


    }

    private function procesorReadInfo()
    {

        $mainsection = $this->getMainsecion(2);
        $section = $this->getSection($mainsection,"id",0);

        $this->procesor['rdzeni'] = (string) $this->getData($section,"Cores");
        $this->procesor['watkow'] = (string) $this->getData($section,"Threads");
        $this->procesor['model'] = (string) $this->getData($section,"Name");

        $this->procesor['technologia'] = (string) $this->getData($section,"Code Name") . " " . $this->getData($section,"Technology");

        $temp = $this->getData($section,"Specification");
        $temp = explode("@",$temp);
        $temp = str_replace(" ","",$temp[1]);
        $temp = str_replace("GHz"," GHz",$temp);

        $this->procesor['bazowaCzestotliwosc'] = (string) $temp;
        $this->procesor['socket'] = (string) $this->getData($section,"Package");

        $cacheSection = $this->getSection($section,"title","Caches");
        $temp = $this->getDataLastCount($cacheSection);
        $temp = (int) str_replace(" KBytes","",$temp);
        $temp = $temp / 1024;

        $this->procesor['cache'] = (string) $temp . " MB";

        $this->procesor['instrukcje'] = (string) $this->getData($section,"Instructions");

    }

    private function ramReadInfo()
    {
        $mainSection = $this->getMainsecion(3);

        // ilosc slotow
        $slotSection = $this->getSection($mainSection,"title","Memory slots");

        $this->ram['iloscSlotow'] = (string) $this->getData($slotSection,"Total memory slots");
        $this->ram['iloscUzywanychSlotow'] = (string) $this->getData($slotSection,"Used memory slots");

        // calowita ilosc pamieci

        $memorySection = $this->getSection($mainSection,"title","Memory");

        $this->ram['typPamieci'] = (string) $this->getData($memorySection,"Type");

        $temp = $this->getData($memorySection,"Size");
        preg_match("/^[0-9]{1,}/",$temp,$found);
        $temp = (int) $found[0];
        $temp = $temp / 1024;

        $this->ram['calkowitaPojemnosc'] = (string) $temp . " GB";

        // moduły zainstalowane

        $this->ram['modulyZainstalowane'] = array();

        $modulySection = $this->getSection($mainSection,"title","SPD");

        for ($x = 1; $x <= $this->ram['iloscUzywanychSlotow']; $x++)
        {
            $sekcja = $this->getSection($modulySection,"title","Slot #".$x);

            $producent = $this->getData($sekcja,"Manufacturer");

            $pojemnosc = $this->getData($sekcja,"Size");
            preg_match("/^[0-9]{1,}/",$pojemnosc,$found);
            $pojemnosc = (int) $found[0] / 1024;
            $pojemnosc = (string) $pojemnosc . " GB";

            $typ = (string) $this->getData($sekcja,"Type");

            $typ2 = (string) $this->getData($sekcja,"Max Bandwidth");

            $this->ram['modulyZainstalowane'][] = (string) $producent . " - " . $pojemnosc . " - " . $typ . " - " . $typ2;

        }

    }

    private function plytaGlownaReadInfo()
    {

        $mainSection = $this->getMainsecion(4);

        $this->plytaGlowna['model'] = (string) $this->getData($mainSection,"Manufacturer") . " " . $this->getData($mainSection,"Model");
        $this->plytaGlowna['chipset'] = (string) $this->getData($mainSection,"Southbridge Vendor") . " " . $this->getData($mainSection,"Southbridge Model");

    }

    private function ekranReadInfo()
    {

        $mainSection = $this->getMainsecion(5);

        for ($x=0; $x < count($mainSection->section);$x++)
        {

            if (preg_match("/Monitor/",$mainSection->section[$x]['title'],$found))
            {
                // znaleziono monitor

                $width = $this->getData($mainSection->section[$x],"Monitor Width");
                $height = $this->getData($mainSection->section[$x],"Monitor Height");
                $odswierzanie = $this->getData($mainSection->section[$x],"Monitor Frequency");

                $this->ekran['rozdzielczoscNatywna'] = (string) $width . " x " . $height . " @ " . $odswierzanie;

            }
            else
            {
                // karta grafiki (prawdopodobnie)

                $producent = (string) $this -> getData($mainSection->section[$x],"Manufacturer");
                $model = (string) $this -> getData($mainSection->section[$x],"Model");

                $this->kartaGraficzna = $producent . " " . $model;

            }

        }

    }

    private function dyskTwardyReadInfo()
    {

        // po eregi wszystko co nie jest Flash drives (title section)

        $mainSection = $this->getMainsecion(6);

        $section = $this->getSection($mainSection,"title","Hard drives");

        if (count($section) > 0)
        {

            for ($x=0; $x < count($section->section); $x++)
            {

                $sectionDysk = $section->section[$x];

                $dyskInfo = array();
                $dyskInfo['model'] = (string) $sectionDysk['title'];
                $dyskInfo['pojemnosc'] = (string) $this->getData($sectionDysk,"Model Capacity For This Specific Drive");
                $dyskInfo['interfejs'] = (string) $this->getData($sectionDysk,"SATA type");
                $dyskInfo['rpm'] = (string) $this->getData($sectionDysk,"Speed");

                $this->dyskTwardy[] = $dyskInfo;

            }


        }
        else
        {
            $this->dyskTwardy = null;
        }



    }

    private function kartaDzwiekowaReadInfo()
    {
        $mainSection = $this->getMainsecion(8);
        $section = $this->getSection($mainSection,"title","Sound Cards");

        $this->kartaDzwiekowa = $this->getDataToArray($section,"title");
    }

    private function kartaSieciowaReadInfo()
    {
        $mainSection = $this->getMainsecion(10);
        $section = $this->getSection($mainSection,"title","Adapters List");
        $section = $this->getSection($section,"title","Enabled");

        for ($x = 0; $x < count($section->section); $x++)
        {
            $this->kartaSieciowa[] = (string) $section->section[$x]['title'];
        }

    }

    private function napedOptycznyReadInfo()
    {

        $mainSection = $this->getMainsecion(7);

        if (count($mainSection->section) == 0)
        {
            $this->napedOptyczny = null;
        }
        else
        {
            if (count($mainSection->section) == 1)
            {
                $this->napedOptyczny[0]['model'] =(string) $this->getData($mainSection->section[0],"Name");

                $this->napedOptyczny[0]['read'] = (string) $this->getData($mainSection->section[0],"Read capabilities");
                $this->napedOptyczny[0]['write'] = (string) $this->getData($mainSection->section[0],"Write capabilities");

            }
            else
            {
                echo "Wiecej niz jeden napedow optycznych";
            }
        }

    }

}