echo '<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<head></head>
<body>';

include "ReportSpeecy.php";
include "ListaWypunktowanaHTML.php";

if (($_POST['action'] == "generate") and !empty($_FILES['everestFile']['tmp_name']))
{

    $file = $_FILES['everestFile']['tmp_name'];

    $raport = new ReportSpeecy($file);


// Przygotowanie listy

    $listaGlowna = new ListaWypunktowanaHTML();

    $listaGlowna->addElement("Producent",$_POST['producent']);
    $listaGlowna->addElement("Model",$_POST['model']);

// Procesor
    $procesor = new ListaWypunktowanaHTML();
    $procesor->addElement("Częstotliwość bazowa",$raport->procesor['bazowaCzestotliwosc'],false);
    $procesor->addElement("Częstotliwość TURBO","",false);
    $procesor->addElement("Ilość rdzeni",$raport->procesor['rdzeni'],false);
    $procesor->addElement("Ilość wątków",$raport->procesor['watkow'],false);
    $procesor->addElement("Pamięć Cache",$raport->procesor['cache'],false);
    $procesor->addElement("Technologia",$raport->procesor['technologia'],false);
    $procesor->addElement("Socket",$raport->procesor['socket'],false);
    $procesor->addElement("Instrukcje procesora",$raport->procesor['instrukcje'],false);

    $listaGlowna->addElement("Procesor",$raport->procesor['model'].$procesor->getHTML());

// Pamiec RAM
    $ram = new ListaWypunktowanaHTML();
    $ram->addElement("Ilość slotów",$raport->ram['iloscSlotow'],false);
    $ram->addElement("Ilość używanych slotów",$raport->ram['iloscUzywanychSlotow'],false);
    $ram->addElement("Typ pamięci",$raport->ram['typPamieci'],false);

    $ramModuly = new ListaWypunktowanaHTML();

    for ($x = 0; $x < count($raport->ram['modulyZainstalowane']); $x++)
    {
        $ramModuly->addElement("Moduł ".($x+1),$raport->ram['modulyZainstalowane'][$x],false);
    }

    $ram->addElement("Moduły zainstalowane",$ramModuly->getHTML(),false);

    $listaGlowna->addElement("Pamięć RAM",$raport->ram['calkowitaPojemnosc'].$ram->getHTML());

// Plyta glowna

    $plytaGlowna = new ListaWypunktowanaHTML();
    $plytaGlowna->addElement("Chipset",$raport->plytaGlowna['chipset'],false);

    $listaGlowna->addElement("Płyta główna",$raport->plytaGlowna['model'].$plytaGlowna->getHTML());

// Ekran

    $ekran = new ListaWypunktowanaHTML();
    $ekran->addElement("Rozdzielczosc natywna",$raport->ekran['rozdzielczoscNatywna'],false);

    $listaGlowna->addElement("Ekran",$ekran->getHTML());

// Karta graficzna

    $listaGlowna->addElement("Karta graficzna",$raport->kartaGraficzna);

// Karta dzwiekowa

    $listaGlowna->addElement("Karta dzwiekowa",$raport->kartaDzwiekowa);

// Dysk twardy

    if ($raport->dyskTwardy == null)
    {
        $listaGlowna->addElement("Dysk twardy","brak");
    }
    else
    {

        if (count($raport->dyskTwardy) > 1)
        {
            echo "dwa dyski";
        }
        else
        {
            $listaDysk = new ListaWypunktowanaHTML();
            $listaDysk ->addElement("Pojemność",$raport->dyskTwardy[0]['pojemnosc'],false);
            $listaDysk ->addElement("Interfejs",$raport->dyskTwardy[0]['interfejs'],false);
            $listaDysk ->addElement("Szybkość obrotowa",$raport->dyskTwardy[0]['rpm'],false);

            $listaGlowna->addElement("Dysk twardy",$raport->dyskTwardy[0]['model'].$listaDysk->getHTML());

        }

    }

// karty sieciowe

    $listaGlowna ->addElement("Komunikacja",$raport->kartaSieciowa);

// naped optyczny

    $listaNaped = new ListaWypunktowanaHTML();
    $listaNaped->addElement("Zapisuje",$raport->napedOptyczny[0]['write'],false);
    $listaNaped->addElement("Czyta",$raport->napedOptyczny[0]['read'],false);

    $listaGlowna -> addElement("Napęd optyczny",$raport->napedOptyczny[0]['model'].$listaNaped->getHTML());


    $listaGlowna->addElement("Bateria",$_POST['bateria']);
    $listaGlowna->addElement("Zasilacz",$_POST['zasilacz']);
    $listaGlowna->addElement("Wymiary",$_POST['wymiary']);
    $listaGlowna->addElement("Waga",$_POST['waga']);
    $listaGlowna->addElement("Inne");

    echo $listaGlowna->getHTML();


}
else
{

    // formularz wstepny - wraz z wyborem pliku EVERESTA

    echo "
            <form action='index.php' method='post' enctype='multipart/form-data'>
            <input type='hidden' name='action' value='generate' />
            <table align='center'>
            <tr>
                <td>Producent:</td>
                <td><input type='text' name='producent' /></td>
            </tr>

            <tr>
                <td>Model:</td>
                <td><input type='text' name='model' /></td>
            </tr>

            <tr>
                <td>Bateria:</td>
                <td><input type='text' name='bateria' /></td>
            </tr>

            <tr>
                <td>Zasilacz:</td>
                <td><input type='text' name='zasilacz' /></td>
            </tr>

            <tr>
                <td>Wymiary:</td>
                <td><input type='text' name='wymiary' /></td>
            </tr>

            <tr>
                <td>Waga:</td>
                <td><input type='text' name='waga' /></td>
            </tr>

            <tr>
                <td>Plik z raportem<br/>(XML)</td>
                <td><input type='file' name='everestFile' id='everestFile' /></td>
            </tr>

            <tr>
                <td></td>
                <td><input type='submit' value='Wyślij' /></td>
            </tr>

            </form>

            ";


}



echo '</body></html>';
